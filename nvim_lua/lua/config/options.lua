-- Remap space as leader key
vim.g.mapleader = " "
vim.g.maplocalleader = " "

-- Enable line number and relative line number
vim.opt.number = true
vim.opt.relativenumber = false

-- Use spaces for tabs
vim.opt.tabstop = 2
vim.opt.shiftwidth = 2
vim.opt.shiftround = true
vim.opt.expandtab = true

-- Enable the sign column to prevent screen from jumping
vim.opt.signcolumn = "yes"

-- Enable system clipboard
vim.opt.clipboard = "unnamed,unnamedplus"

-- Clear last search highlight
vim.keymap.set("n", "<leader>h", ":nohlsearch<CR>")
