return {
	{
		"williamboman/mason.nvim", -- NOTE: Must be loaded before other
		dependencies = {
			"WhoIsSethDaniel/mason-tool-installer.nvim",
		},
		config = function()
			local status_ok, mason = pcall(require, "mason")
			if not status_ok then
				return
			end
			mason.setup({})
			local status_ok, mason_tool = pcall(require, "mason-tool-installer")
			if not status_ok then
				return
			end

			local servers = {
				clangd = {},
				pyright = {},
				rust_analyzer = {
					cmd = {
						"rustup",
						"run",
						"stable",
						"rust-analyzer",
					},
				},
			}
			local ensure_installed = vim.tbl_keys(servers or {})
			vim.list_extend(ensure_installed, {
				"stylua", -- Used to format Lua code
			})
			mason_tool.setup({
				ensure_installed = ensure_installed,
			})
		end,
	},
	{
		"neovim/nvim-lspconfig",
		dependencies = {
			"williamboman/mason-lspconfig.nvim",

			-- Status updates for LSP, like indexing status etc.
			-- NOTE: `opts = {}` is the same as calling `require('fidget').setup({})`
			{ "j-hui/fidget.nvim", opts = {} },

			-- Allows extra capabilities provided by nvim-cmp
			"hrsh7th/cmp-nvim-lsp",
		},
		config = function()
			local status_ok, lspconfig = pcall(require, "lspconfig")
			if not status_ok then
				return
			end
			local capabilities = vim.lsp.protocol.make_client_capabilities()
			capabilities = vim.tbl_deep_extend("force", capabilities, require("cmp_nvim_lsp").default_capabilities())
			lspconfig.clangd.setup({
				capabilities = capabilities,
			})
			lspconfig.pyright.setup({
				capabilities = capabilities,
			})
			lspconfig.lua_ls.setup({
				settings = {
					Lua = {
						diagnostics = {
							globals = { "vim" },
						},
					},
				},
			})
			lspconfig.rust_analyzer.setup({
				capabilities = capabilities,
			})
			-- mappings

			local builtin = require("telescope.builtin")
			vim.keymap.set("n", "<leader>gd", builtin.lsp_definitions, {})
			vim.keymap.set("n", "<leader>gD", vim.lsp.buf.declaration, {})
			vim.keymap.set("n", "<leader>gr", builtin.lsp_references, {})
			vim.keymap.set("n", "<leader>D", builtin.lsp_type_definitions, {})
			vim.keymap.set("n", "<leader>bs", builtin.lsp_document_symbols, {})
			vim.keymap.set("n", "<leader>ps", builtin.lsp_dynamic_workspace_symbols, {})
			vim.keymap.set("n", "<leader>rn", vim.lsp.buf.rename, {})
			vim.keymap.set("n", "<leader>ca", vim.lsp.buf.code_action, {})
		end,
	},
}
