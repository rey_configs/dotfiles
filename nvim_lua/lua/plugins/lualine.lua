return {
	"nvim-lualine/lualine.nvim",
	opts = {
		options = {},
	},
	config = function()
		local status_ok, lualine = pcall(require, "lualine")
		if not status_ok then
			return
		end
		lualine.setup({
			icons_enabled = false,
			theme = "onedark",
			component_separators = "|",
			section_separators = "",
		})
	end,
}
