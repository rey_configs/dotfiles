return {
	"stevearc/oil.nvim",
	name = "oil",
	lazy = false,
	opts = {},
	-- Optional dependencies
	dependencies = { "nvim-tree/nvim-web-devicons" },
	config = function()
		local status_ok, oil = pcall(require, "oil")
		if not status_ok then
			return
		end
		oil.setup({
			default_file_explorer = true,
			view_options = {
				show_hidden = true,
			},
		})
		-- mappings
		vim.keymap.set("n", "<Leader>e", oil.toggle_float, { desc = "Open parent directory" })
	end,
}
