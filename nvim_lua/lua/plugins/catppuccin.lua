return {
	"catppuccin/nvim",
	name = "catppuccin",
	lazy = false,
	priority = 1000,
	config = function()
		local status_ok, catpuccin = pcall(require, "catppuccin")
		if not status_ok then
			return
		end
		catpuccin.setup({
			flavour = "mocha",
			--flavour = "macchiato",
		})
		vim.cmd.colorscheme("catppuccin")
	end,
}
