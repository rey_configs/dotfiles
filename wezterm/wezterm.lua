-- Pull in the wezterm API
local wezterm = require("wezterm")
local act = wezterm.action
-- This table will hold the configuration.
local config = {}

-- In newer versions of wezterm, use the config_builder which will
-- help provide clearer error messages
if wezterm.config_builder then
	config = wezterm.config_builder()
end

-- This is where you actually apply your config choices
--config.hide_tab_bar_if_only_one_tab = true
config.enable_tab_bar = true

-- Set tabs keymappings
config.keys = {
	{
		key = "t",
		mods = "ALT",
		action = act.SpawnTab("CurrentPaneDomain"),
	},
}
for i = 1, 8 do
	table.insert(config.keys, {
		key = tostring(i),
		mods = "ALT",
		action = act.ActivateTab(i - 1),
	})
end

-- For example, changing the color scheme:
config.color_scheme = "Catppuccin Macchiato"
config.color_scheme = "Catppuccin Mocha"
config.font = wezterm.font_with_fallback({
	"Hack",
	"Fira Code",
})
--config.font_size = 12

-- and finally, return the configuration to wezterm
return config
