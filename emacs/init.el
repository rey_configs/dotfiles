;;====== Setup package system ======

(require 'package)
(setq package-archives '(("melpa" . "https://melpa.org/packages/")
                         ("org" . "https://orgmode.org/elpa/")
                         ("elpa" . "https://elpa.gnu.org/packages/")))
(package-initialize)
;; Initialize use-package
(unless (package-installed-p 'use-package)
  (package-install 'use-package))
(require 'use-package)
(setq use-package-always-ensure t)      ; Set :ensure t for each package by default
;;(setq use-package-verbose t)          ; Get a reading on packages that get loaded at startup


;;====== General settings ======

(setq inhibit-startup-message t)        ; Disable startup message
;; (menu-bar-mode -1)                   ; Disable the menu bar
(tool-bar-mode -1)                      ; Disable the toolbar
(setq-default tab-width 2               ; Set tab indent to 2 spaces
              indent-tabs-mode nil)     ; Use space instead of tabs
(set-default-coding-systems 'utf-8)     ; Use UTF8 be default
(setq make-backup-files nil)            ; Do not make backup files

(column-number-mode)			; Show line numbers
(dolist (mode				; Show line numbers for below modes
   '(text-mode-hook prog-mode-hook conf-mode-hook))
  (add-hook mode (lambda () (display-line-numbers-mode 1))))
(dolist (mode				; Disable line nubmers in below modes
   '(org-mode-hook eshell-mode-hook))
  (add-hook mode (lambda() (display-line-numbers-mode 0))))

(fset 'yes-or-no-p 'y-or-n-p)                                 ; Change yes/no to y/n
(windmove-default-keybindings)                                ; Change windows with Shift+arrows

(setq gc-cons-threshold (* 50 1000 1000)) ; The default is 800 kilobytes ( Measured in bytes )

(add-hook 'emacs-startup-hook             ; Profile emacs startup
          (lambda ()
            (message "*** Emacs loaded in %s with %d garbage collections."
                     (format "%.2f seconds"
                             (float-time
                              (time-subtract after-init-time before-init-time)))
                     gcs-done)))


;;====== Emacs internal packages ======

(use-package delsel                     ; Replace selected region by typing new text
  :ensure nil
  :config
  (delete-selection-mode 1))

(use-package paren                     ; Highlight matching brackets
  :ensure nil
  :config
  (show-paren-mode 1))

(use-package whitespace                 ; Remove whitespaces on save
  :ensure nil
  :hook
  (before-save . whitespace-cleanup))

(use-package ediff                      ; Open buffers side-by-side
  :ensure nil
  :config
  (setq ediff-window-setup-function #'ediff-setup-windows-plain)
  (setq ediff-split-window-function #'split-window-horizontally))

(use-package dired                      ; Remove intermediate dired windows
  :ensure nil
  :config
  (setq delete-by-moving-to-trash t)
  (eval-after-load "dired"
    #'(lambda ()
        (put 'dired-find-alternate-file 'disabled nil)
        (define-key dired-mode-map (kbd "RET") #'dired-find-alternate-file))))

(use-package scroll-bar                 ; Disable visible scrollbar
  :ensure nil
  :config
  (scroll-bar-mode -1))

(use-package tooltip                    ; Disable tooltips
  :ensure nil
  :config
  (tooltip-mode -1))

(use-package fringe                     ; Add left/right margins
  :ensure nil
  :config
  (set-fringe-mode 5))

(use-package frame                      ; Start frame maximilized
  :ensure nil
  :config
  (setq initial-frame-alist '((fullscreen . maximized))))

(use-package files                      ; Don't create backups
  :ensure nil
  :config
  (setq confirm-kill-processes nil      ; Don't confirm process kill
        create-lockfiles nil            ; Don't create .# files
        make-backup-files nil))         ; Don't create .~ files

(use-package magit
  :bind ("C-x g" . magit-status))

(use-package org
  :defer t
  :pin org                              ; Use org repo instead of melpa (should have move up to date packages)
  :hook ((org-mode . org-indent-mode))
  :config
  (setq org-agenda-files                ; Set files for agenda view
        '("~/org/gtd/next.org"
          "~/org/gtd/reccuring.org"))
  ;; TODO: rethink gtd workflow
  (setq org-todo-keywords               ; Set todo keywords ( all_todo_states | all_finished_states )
        '((sequence "TODO(t)" "NEXT(n)" "WAIT(w)" "PROJ(p)" "|" "DONE(d)" "CANCALLED(c)")))
  (setq org-refile-targets  ; reflow inbox item possibilities
  '(
    ("~/org/gtd/next.org" :maxlevel . 2)
    ("~/org/gtd/someday.org" :level . 2)
          ("~/org/gtd/recurring.org" :maxlevel . 3))
   )
  (setq org-capture-templates ; capture templates
  '(("t" "TODO [inbox]" entry (file "~/org/gtd/inbox.org") "* TODO %i%?")
   ; ("g" "Groceries" entry (file "~/org/gtd/groceries.org") "* %i%? \n %U")
    ))
  )

;; (dolist (face '((org-level-1 . 1.2)
;;                 (org-level-2 . 1.1)
;;                 (org-level-3 . 1.05)
;;                 (org-level-4 . 1.0)
;;                 (org-level-5 . 1.1)
;;                 (org-level-6 . 1.1)
;;                 (org-level-7 . 1.1)
;;                 (org-level-8 . 1.1))))

;; (use-package org-bullets
;;   :hook (org-mode . org-bullets-mode))


;;====== Packages ======

(use-package doom-themes		; Theme
  :defer t
  :init
  (load-theme 'doom-nord t))

(use-package doom-modeline		; Powerline status bar
  :init
  (doom-modeline-mode 1)
  :custom
  (doom-modeline-buffer-file-name-style 'truncate-except-project)
  (doom-modeline-lsp t))

(use-package all-the-icons)		; Adds icon support

(use-package undo-tree			; Visualize undo
  :defer t
  :diminish undo-tree-mode
  :config
  (progn
    (global-undo-tree-mode 1)
    (setq undo-tree-visualizer-timestamps t)
    (setq undo-tree-visualizer-diff t)))

(use-package rainbow-delimiters		; See matching delimiters
  :hook 'prog-mode . 'rainbow-delimiters-mode)

(use-package which-key			; Show help for key shortcuts
  :init
  (which-key-mode)
  :diminish which-key-mode
  :config
  (setq which-key-idle-delay 0.3))

(use-package ivy			; Better minibuffer with descriptions and shortcuts
  :diminish
  :bind
  (("C-s" . swiper)                 ; better search
  :map ivy-minibuffer-map
  ("TAB" . ivy-alt-done)
  :map ivy-switch-buffer-map
  ("C-d" . ivy-switch-buffer-kill)) ; close buffers while showing them
  :init
  (ivy-mode 1)
  :config
  (setq ivy-use-virtual-buffers t)
  (setq ivy-wrap t)
  (setq ivy-count-format "(%d/%d) ")
  (setq enable-recursive-minibuffers t))

(use-package ivy-rich                   ; better interface/visuals for ivy (minibuffer)
  :init
  (ivy-rich-mode 1)
  :after counsel)

(use-package counsel                    ; Better visuals for search reasults
  :demand t
  :bind
  (("M-x" . counsel-M-x)
  ;; ("C-x b" . counsel-ibuffer)
  ("C-x C-f" . counsel-find-file)
  ("C-x f" . counsel-find-file))
  :config
  (setq ivy-initial-inputs-alist nil)) ;; dont start searches with ^


;;====== Development support packages


(use-package flycheck
  :config
  (global-flycheck-mode 1))

(use-package company
  :diminish company-mode
  :hook (prog-mode . company-mode)
  :config
  (setq company-minimum-prefix-length 1
        company-idle-delay 0.1
        company-selection-wrap-around t
        company-tooltip-align-annotations t
        company-frontends '(company-pseudo-tooltip-frontend ; show tooltip even for single candidate
                            company-echo-metadata-frontend))
  (define-key company-active-map (kbd "C-n") 'company-select-next)
  (define-key company-active-map (kbd "C-p") 'company-select-previous))

(use-package web-mode                   ; Add major mode for HTML/CSS/JSON/JS
  :mode (("\\.html?\\'" . web-mode)
         ("\\.css\\'"   . web-mode)
         ("\\.jsx?\\'"  . web-mode)
         ("\\.tsx?\\'"  . web-mode)
         ("\\.json\\'"  . web-mode))
  :config
  (setq web-mode-markup-indent-offset 2) ; HTML
  (setq web-mode-css-indent-offset 2)    ; CSS
  (setq web-mode-code-indent-offset 2)   ; JS/JSX/TS/TSX
  (setq web-mode-content-types-alist '(("jsx" . "\\.js[x]?\\'"))))
